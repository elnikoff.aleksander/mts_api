# -*- coding: utf-8 -*-


import subprocess
import datetime
import os
from pathlib import *

# формирование абсолютного пути родительской директории для запуска скрипта .py из любого места системы (Windows/Linux)
path_file = Path(__file__)
path_parents = path_file.parents[0]
saved_attachments = Path(path_parents, 'saved_attachments')

now = str(datetime.datetime.today().strftime('%d.%m.%Y'))
print('поиск .rar файлов в каталоге....', saved_attachments)


def unpack_archive():
    """функция распаковки архивов в папке saved_attachments(для linux).
    для windows нужно качать отдельную библиотеку с сайта https://www.rarlab.com/ и подкладывать в каталог с python"""
    for folder, sub, files in os.walk(saved_attachments):
        for file in files:
            if file.endswith('.rar'):
                path_archive = (os.path.join(saved_attachments, file))
                subprocess.run('unar -D -o {} {}'.format(saved_attachments, path_archive), shell=True)
                os.remove(path_archive)
        else:
            print('файлов .rar не найдено, или все распакованы... ')
            break


unpack_archive()
