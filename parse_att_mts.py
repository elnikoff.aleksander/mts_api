# -*- coding: utf-8 -*-
import datetime
import math
import os
import xml.etree.ElementTree as ET
from pathlib import *
import requests
from send_mail_report import *
from class_db import *

from parse_mts.class_db import connection_db, ExpensesMobileNumber

now = str(datetime.datetime.today().strftime('%d.%m.%Y'))

# формирование абсолютного пути родительской директории для запуска скрипта .py из любого места системы
path_file = Path(__file__)
path_parents = path_file.parents[0]
saved_attachments = Path(path_parents, 'saved_attachments')
archive_file = Path(saved_attachments, 'archive_file')
archive_file_now = Path(saved_attachments, 'archive_file', '{}'.format(now))
print('поиск файлов в директории -', saved_attachments)

# cписок пользователей получающих уведомления
subscriber = {111111111: 'name', }


def send_message(sub_one, text):
    """функция отправки передаваемого текста подписчику в чат бот"""
    token = 'token bot'  # ввести токен своего бота
    method = '/sendMessage'
    text = text
    send_response = requests.post(
        url=str('https://api.telegram.org/bot' + token + method),
        data={'chat_id': sub_one,
              'text': text}
    ).json()



def parse_att():
    with connection_db:
        connection_db.create_tables([ExpensesMobileNumber])
    for folder, sub, files in os.walk(saved_attachments):
        for file in files:
            saved_attachments_file = Path(path_parents, 'saved_attachments', '{}'.format(file))
            if file.endswith('.xml'):
                print('найден файл', file)
                tree = ET.parse(saved_attachments_file)
                root = tree.getroot()
                a = 0
                for tp in root.iter('tp'):
                    expenses_a = tp.attrib['a']
                    owner_u = tp.attrib['u']
                    number_n = tp.attrib['n']
                    number_sim = tp.attrib['sim']
                    expenses_a_int = float('.'.join((expenses_a.split(','))))
                    expenses_a = math.floor(expenses_a_int)
                    a += 1
                    for ts in tp.iter('ts'):
                        tarif_name = ts.attrib['n']
                    for ss in tp.iter('ss'):
                        first_date_tarif_str = ss.attrib['sd']
                        first_date_tarif = first_date_tarif_str[0:10]
                        break
                    print(a, owner_u, number_n, number_sim, expenses_a, tarif_name, first_date_tarif)
                    with connection_db:
                        ExpensesMobileNumber.create(name_number=owner_u, mobile_number=number_n,
                                                    sim_number=number_sim, expenses_number=expenses_a)
                    if expenses_a_int > 5000:
                        text = "Абонент {} с номером {} имеет перерасход {} руб. на {}г. Тариф: {}. Начало периода {}".format(owner_u, number_n, expenses_a, now, tarif_name, first_date_tarif)
                        for sub_one in subscriber:
                            send_message(sub_one, text)
            print('Информация по номерам имеющим расходы отправлена в чат телеграмм')
        else:
            print('все файлы .xml обработаны или отсутствуют в каталоге')
        break

parse_att()


# удаление файлов после парсинга
def remove_xml():
    for folder, sub, files in os.walk(saved_attachments):
        for file in files:
            saved_attachments_file = Path(path_parents, 'saved_attachments', '{}'.format(file))
            if file.endswith('.xml'):
                os.remove(saved_attachments_file)
        else:
            print('-----------------------------------------------------------')
            print('все файлы обработы и удалены')
            break


remove_xml()
