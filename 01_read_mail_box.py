# -*- coding: utf-8 -*-

import os
import email
import imaplib
from pathlib import *

# формирование абсолютного пути родительской директории для запуска скрипта .py из любого места системы (Windows/Linux)
path_file = Path(__file__)
path_parents = path_file.parents[0]
saved_attachments = Path(path_parents, 'saved_attachments')

''' Чтобы подключения к почтовому серверу, нужнто чтобы он был доступен из внешней сети. Или при подлючении к VPN.
Иначе будет "TimeoutError: [WinError 10060] "
'''

# данные учетной записи для подключения к почтовому серверу
mail = imaplib.IMAP4_SSL('mail server')  # менять на свои данные
mail.login('login', 'password')  # менять на свои данные
mail.list()
mail.select('inbox')


def read_mail_and_save_att():
    """функция чтения почтового ящика и сохранения вложений в saved_attachments"""
    i = 0
    # Если в письме будет более 1-го вложения, то сохранится только первое.
    while i < 100:
        result, data = mail.uid('search', None, "ALL")
        try:
            latest_email_uid = data[0].split()[-1]
        except IndexError:
            print('в почтовом ящике сообщений не найдено')
            break

        result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
        raw_email = data[0][1]

        try:
            email_message = email.message_from_string(raw_email)
        except TypeError:
            email_message = email.message_from_bytes(raw_email)
            print('найдено письмо от', email.header.make_header(email.header.decode_header(email_message['From'])))

        for part in email_message.walk():
            if "application" in part.get_content_type():
                filename = part.get_filename()
                filename = str(email.header.make_header(email.header.decode_header(filename)))
                print("найдено вложение", filename, "сохранили в каталог", saved_attachments)
                fp = open(os.path.join(saved_attachments, filename), 'wb')
                fp.write(part.get_payload(decode=1))
                fp.close
        mail.uid('STORE', latest_email_uid, '+FLAGS', '(\Deleted)')
        print('удалили письмо от', email.header.make_header(email.header.decode_header(email_message['From'])))
        mail.expunge()


read_mail_and_save_att()
