# -*- coding: utf-8 -*-
from peewee import *
import datetime


"""модуль описывающий соединение с БД, таблицы и ее типы данных"""

# используется для формирование записи в БД в сокращенном формате времени
date_update_full = str(datetime.datetime.today().strftime('%Y.%m.%d_%H:%M:%S'))

# соединение с БД
connection_db = PostgresqlDatabase('name_db', user='user_db',
                                   password='password', host='host', autoconnect=False)

# общий класс для всех таблицы(на текущий момент оставлена только одна)
class BaseModel(Model):
    id = AutoField(unique=True)
    update_time = DateTimeField(default=date_update_full)

    class Meta:
        database = connection_db
        order_by = 'id'

# класс таблицы расходов
class ExpensesMobileNumber(BaseModel):
    name_number = CharField()
    mobile_number = CharField()
    sim_number = CharField()
    expenses_number = IntegerField()
