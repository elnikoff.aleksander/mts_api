# -*- coding: utf-8 -*-
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def send_mail_report_details(mail_subject, mail_body):
    """функция отправки сообщений на адрес электронной почты"""
    smtp_host = 'mail server'  # ввести адрес постового сервера
    user_server = 'user'  # ввести учетную запись для подключения к серверу
    user_server_pass = 'password'  # ввести пароль от учетной записи
    mail_subject = mail_subject
    mail_from = 'e-mail'  # ввести отправителя сообщения
    mail_to = 'e-mail'  # ввести адрес получателя сообщения
    mail_msg = MIMEMultipart()
    mail_msg['From'] = mail_from
    mail_msg['To'] = mail_to
    mail_msg['Subject'] = mail_subject
    mail_body = mail_body
    mail_msg.attach(MIMEText(mail_body, 'plain'))
    mail_server = smtplib.SMTP(host=smtp_host, port='465')
    mail_server.starttls()
    mail_server.login(user=user_server, password=user_server_pass)
    mail_server.send_message(mail_msg)
    mail_server.quit()